Quick start
===========

Follow these three steps to get started.

Step 1
------
Install Autheos by adding the embed code to the `<head>` tag of your website:

```html
<script src="//cdn.autheos.com/code/embedcode@^2.0.0"></script>
```
> Read the [installation][installing] instructions for different installation
> options.

Step 2
------
Install add-to-chart snippet*

```html
<script>
Autheos.attachListener('add-to-cart', [
  document.querySelector('#your-own-add-to-basket-selector-here')
]);
</script>
```
Of course, replace replace `#your-own-add-to-basket-selector-here`
with your own add-to-basket selector.

*Free users: if your shop uses Autheos for free this step is mandatory. Do not worry! We aggregat and anonimise all the data into one conversion data pool to measure the effect of video quality on conversion. 
> For more information on how we use your data, read about [conversion tracking][conversion-tracking].

Step 3
------
Product-page set up

Place the following snippet anywhere in your product page where you'd like to see
our video button. Don't forget to include a language filter to specify the
languages for your market:

```html
<script>
Autheos.button({
  ean: '1234567890123',
  language: ['nl', 'en']
});
</script>
```

- replace `1234567890123` with the EAN of the product you want to show a video
  for;
- replace `nl` and/or `en` with the languages you want to display. You can list any number of laguages there. 

> For more info, details and languages we support click [here][embedding-videos].


If you have no access to EANs on your product page please [contact us][support] at
[support@autheos.com][mailto].

Step 4
------------
Add videos to your [search results, brand and custom pages][embedding-videos].
Available only to e-taillers in premium tiers. 


What's next?
------------
To find out what else you can do with Autheos code read our full [API reference][api-reference]

[installing]: 02-installing.md
[embedding-videos]: 03-embedding-videos.md
[conversion-tracking]: 05-conversion-tracking.md
[api-reference]: 11-full-api-reference.md
[support]: 13-support.md
[mailto]: mailto:support@autheos.com
