Support
=======

Got stuck and need some help? Do you have a suggestion or feature request? Don't
hesitate to contact us at [support@autheos.com][mailto]. You can also reach us
via chat on our [website][homepage].

Make sure to read our FAQ below; there is a good chance it answers your
questions!

FAQ
---

### How do I install Autheos in a CMS? (Magento, Wordpress, etc.)
As it currently stands, we have no dedicated plugins for Magento or Wordpress.
You can however still install because there is a way to bla bla

### I don't use EANs on my webshop. What can I do?
Contact us! We're sure we can help you out.

### Do I have to place the embed code everywhere on my webshop?
You can place the embed code anywhere you want. We advice you to only place our
embed code in your templates though, so you can get the EAN of a product from
your backend. Saves you a lot of time, and only requires you to integrate once:

```php
<script>
Autheos.button({
  ean: '<?php echo $ean; ?>',
  language: ['nl', 'en']
});
</script>
```

### I'm a brand with a webshop, can I still use the embed code?
Of course! Autheos works anywhere.

### There is a video on my webshop that I want to remove. What can I do?
Register an account on our platform, go to your dashboard, block the video.

### I want to get more videos on my shop!
Invite brands, upload videos yourself, or import a YouTube channel!

### Why does Autheos track video activity?
We do this to improve user engagement and help us improve our content.

### I don't use the carousel and don't want the code in my bundle. How do I remove that?
If you're using webpack, you can exclude code:
```javascript
new webpack.IgnorePlugin(/^\.\/ui$/, /autheos$/)
```

### Can I use Autheos outside of the browser?
We currently do not support the use of our platform outside of the browser.

### My question is still unanswered :(
Take a look at our general/dashboard/whatever FAQ. You can also reach us at
support@autheos.com. We usually reply within a workday.
http://www.autheos.com/support/
