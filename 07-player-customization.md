Player customization
--------------------

To edit the logo of your player, or the colors, you can edit configuration
```javascript
Autheos.defaults.player.logo = 'http'
...
```

This isn't a great approach. Hard to maintain, it would be better if this was
primarily controlled through the dashboard. Need to think about a proper API
for this feature though, for the time we do not provide this functionality from
our dashboard.
