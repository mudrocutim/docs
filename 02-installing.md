Installing
==========

Our embed code is available as a prepackaged version, and on npm.

Prepackaged browser version
---------------------------

The quickest way to get started is by using our CDN package:
```html
<script src="//cdn.autheos.com/code/embedcode@^2.0.0"></script>
```
> The URL follows [semver], so you can edit the URL for version pinning.

This will expose `Autheos` to the global scope, so you can use the embed code:

```html
<script>
Autheos.button(...)
</script>
```

Package managers
----------------

You can also install our embed code using [Yarn][yarnpkg]:

```bash
yarn add autheos-embedcode
```

Or using [npm][npmjs]:

```bash
npm install --save autheos-embedcode
```

You can now import the embed code like this:

```javascript
import Autheos from 'autheos-embedcode';
Autheos.button(...);
```

Or using `require`:

```javascript
const Autheos = require('autheos-embedcode');
Autheos.button(...);
```

[semver]: https://docs.npmjs.com/getting-started/semantic-versioning
[yarnpkg]: https://yarnpkg.com
[npmjs]: https://www.npmjs.com
