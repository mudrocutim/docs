Migrating from 1.0
==================

because api is different we provide a migration path. but because migrating is
annoying, we also have a compatiblity module

include this with our embed code to keep using the embedcode like you did with
1.0 and shit

```html
<script src="//cdn.autheos.com/code/embedcode@^2.0.0"></script>
<script src="//cdn.autheos.com/code/embedcode-compat@^2.0.0"></script>
```

if youre using npm just install the compat module and use that instead

```bash
yarn add @autheos/embedcode-compat
# or
npm install --save @autheos/embedcode-compat
```

so

```javascript
import Autheos from '@autheos/embedcode';
// or
const Autheos = require('@autheos/embedcode');
```

becomes

```javascript
import Autheos from '@autheos/embedcode-compat';
// or
const Autheos = require('@autheos/embedcode-compat');
```

Not using our embed code?
-------------------------

Maybe you're on a very old implementation of our system. We do not support
custom implementations as of August 2017. We ask you to upgrade your
implementation as our deprecated APIs are will be discontinued in May 2018. Not
doing so _will_ result in a disconnect from out platform.

Please contact us if you need any assistance. We'll be more than happy to help.

Upgrading
---------

To upgrade, update your embed code
just repeat instructions here

instances:
no longer use new Autheos, use Autheos.createInstance

embed function:
this is now basically the same as Autheos.button
except target argument is now the second or third argument instead of first

carousel:
we now use our own carousel so read [using the carousel] for information about
that, there is no one size fits all migration path for stupid magnific popup

custom renderer:
we dont use this anymore, read [creating a custom renderer]

ab testing:
we dont use instance.abTestingEnabled anymore, you can use
Autheos.defaults.enableABTesting instead
read enabling ab testing for more information

getvideos:
this changed only a little bit
video.embed is now Autheos.embedVideo(video)
playerurl should not be used and is deprecated
results now come inside an object that also tells you what was matched to what
videos

if you did anything else, you did something we dont officially support which is
pretty stupid so youre fucked
no jk just contact us because were kind enough to help you <3
