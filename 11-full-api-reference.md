Full API reference
==================


createInstance
--------------

```javascript
Autheos.createInstance([options]);
```

### Arguments
- `options` _(Object)_: A configuration object. You can find all options in
  [configuring the embed code][configuration]. The configuration is merged with
  the configuration of the instance `createInstance` was called on.

### Returns
Returns a new instance of the embed code.

---

button
------

use this to embed a button
```javascript
Autheos.button(search, [target], [options]);
```

### Arguments
- `search` _(Object)_: An object containing search params. Read [somewhere] what
  you can use here.
- `target` _(DOM node)_: A target element to place the button in.
- `options` _(Object)_: Custom embedding options. Accepts the same options as
  Autheos.createInstance.

> The arguments `target` and `options` can be used in either order.<br/>
>`Autheos.button(search, [options], [target]);` works too.

### Returns
Returns a promise that resolves when the button is embedded and shown.

---

thumbnail
---------

use this to embed a button
```javascript
Autheos.thumbnail(search, [target], [options]);
```
Works the same as Autheos.button except its for thumbnails.

---

inline
------

use this to embed a button
```javascript
Autheos.inline(search, [target], [options]);
```
Works the same as Autheos.button except its for inline videos.

---

deferredButton
--------------

Works the same as Autheos.button again except its deferred.
Call Autheos.invokeDeferred to run requests.

---

deferredThumbnail
-----------------

Works the same as Autheos.button again except its deferred.
Call Autheos.invokeDeferred to run requests.

---

deferredInline
--------------

Works the same as Autheos.button again except its deferred.
Call Autheos.invokeDeferred to run requests.

---

invokeDeferred
--------------

Invokes all deferred calls.
Returns a promise that is resolved when all deferred calls are embedded and
shown.

---

embedVideo
----------

Direct embed.

---

createVideo
-----------

Creates a video for embedding.

---

attachListener
--------------

Attaches event listeners.

---

trackEvent
----------

Do custom tracking.

[configuration]: 10-configuration.md
