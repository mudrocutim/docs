Carousel customization
----------------------

If you just want to edit the css, take a look here:
```css
some css properties {}
```

you can also edit the html template, but make sure to not change the classnames
or the embed code won't understand what to do
```javascript
Autheos.defaults.carouselTemplate = '<some html template>'
```

if you dont want to use our carousel, you can replace the render function with
your own, read advanced rendering for more info on that subject

The carousel still being so exposed is maybe a problem. I just have no clue how
to make a sane architecture that's easy for people to implement. Upside is that
it now has a very, very small footprint.
