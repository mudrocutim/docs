Enabling A/B testing (for Dynamic Video Optimization)
=====================================================

E-tailers accepted into the Autheos Lab program have advanced access to our
Dynamic Video Optimization product, which optimizes video content for sales
conversion at your digital point of sale.

A building block of this product is the ability to A/B test (i.e., split test)
various video experiences with your site visitors, to identify which are most
effective in achieving your conversion goals.

To enable A/B testing, insert the following snippet in the embed code:

```javascript
Autheos.defaults.enableABTesting = true;

// now use it
Autheos.button(...);
```

or with a custom instance, in case we should point that out:

```javascript
const instance = Autheos.createInstance({
  enableABTesting: true
});

// now use it
instance.button(...);
```

Make sure to enable conversion tracking, otherwise a/b testing is pointless:
```javascript
Autheos.attachListener('add-to-cart', [
  document.querySelector('#your-own-add-to-basket-selector-here')
]);
```

If you are not a partner of Autheos Lab but are interested in the Dynamic Video
Optimization product, please [contact us](mailto:lab@autheos.com) to determine
your eligibility.
