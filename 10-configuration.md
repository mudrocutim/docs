Configuration
-------------

You can change Autheos config defaults like so:

```javascript
Autheos.defaults.someProperty = 'someValue';
```

or by creating a custom instance:

```javascript
const instance = Autheos.createInstance({
  someProperty: 'someValue'
});
```

In case of creating a custom instance, the parent's config will be inherited and
merged with the new config.

Available configuration properties:

```javascript
// Global defaults, use Autheos.createInstance otherwise. Might want to call it
// something like config instead, as custom instances also have these props?
// Example for when we utilize authorization keys:
Autheos.defaults.key = '123456';

// Enable or disable AB testing. Should this make it to 2.0 even? I still think
// this needs to be in our backend. At least the random factor and type testing
// settings, because that's just going to create a mess here.
Autheos.defaults.enableABTesting = false;

// Enable or disable tracking. Some shops really do not want to send any data,
// so we give them an option to entirely disable tracking. This effectively
// just nulls Autheos.trackEvent.
Autheos.defaults.enableTracking = true;

// Easy embed functions run on top of a template, modify them if you want.
// Editing these templates is especially useful when you want to insert elements
// in an existing image carousel for example, or to give you button a custom
// look and feel like Bol does.
//
// If you wish to use the built-in embed code carousel, leave the existing class
// names in place. The embedcode will automatically assign click handlers to all
// elements with Autheos classnames.
//
// Not sure if we should create a very minimal templating engine that supports
// a simple loop, and string substitution by key, in mustache syntax.
// Including all of mustache is probably overkill, though it's only about
// 5-10kB. Also depends a bit on how good webpack/rollup are in stripping dead
// code. I consider pre-bundled size less important than size after bundling
// with webpack/rollup. Unused code is bad!
Autheos.defaults.buttonTemplate = '<some html with template vars>';
Autheos.defaults.thumbnailTemplate = '<some html with template vars>';
Autheos.defaults.inlineTemplate = '<some html with template vars>';

// By default, button and thumbnails render to a carousel. Edit this variable
// to render to a custom element, like where the image is normally displayed.
Autheos.defaults.videoTarget = document.getElementById('foo');

// For even more advanced configurations, edit the render function. This
// allows you to still use the button, thumbnail and inline functions but
// build a custom renderer. The type argument can be button, thumbnail or
// inline. The results argument is an object retrieved from Autheos.getVideos.
// You'll have to render the button and carousel yourself, or use internal
// embed code functions. Means we need to standardize internal functions. Not
// sure how yet.
Autheos.defaults.render = (type, results) => {};

// We should somehow be able to configure video settings. The following is not a
// final approach. It kinda sucks even. This is getting too complex.
Autheos.defaults.player = {};

// Enable or disable like/dislike buttons. Bol for instance doesn't seem to mind
// generic tracking of videoplayer events, but doesn't like the feedback buttons
// in the video player. This is to allow shops to disable that.
Autheos.defaults.player.enableFeedback = false;

// Enable or disable autoplay when video is embedded:
Autheos.defaults.player.enableAutoplay = false;

// URL to webshop logo:
Autheos.defaults.player.logoUrl = 'http';

// Logo offsets. Perhaps this should be done with CSS instead?
Autheos.defaults.player.logoPosition = 'topright';
Autheos.defaults.player.logoOffsetX = '10px';
Autheos.defaults.player.logoOffsetY = '10px';

// As we're still dealing with an iframe, allow a shop to link to a custom
// stylesheet. I'm very much against this idea because:
// - Requires shops to have a separate stylesheet, instead of one bundled with
//   the rest of their stylesheets. This is a technical burden that usually
//   cannot be solved by simple people.
// - Creates VideoJS specific code, along with our own CSS. This essentially
//   makes it very hard to change anything about the video player at all,
//   because there are a lot of things that we might then break on webshops.
// My proposal: Allow shops to edit the look and feel or their player through
// our dashboard. There they can attach a custom stylesheet, or just edit a
// couple of variables. This makes it easy for noobs to style their player
// without any technical expertise, plus it allows us to update the videoplayer
// without being interfered by specific user stylesheets. Here people can also
// pick their logo, position the logo and pick colors or templates. A custom
// stylesheet would only be for advanced people, with a warning. Either way we
// could keep this setting for that purpose. Perhaps we can do the same for JS?
Autheos.defaults.player.stylesheetUrl = 'http';
Autheos.defaults.player.scriptUrl = 'http';
```
