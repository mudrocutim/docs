Embedding videos
================

We provide various different ways to embed videos on your webshop pages. You
can place a button or a thumbnail that will open a carousel, or place videos
inline into your pages.

You can implement videos on any page you want, below are instructions for
product pages and search results pages.

For product pages
-----------------

To embed a button:
```javascript
Autheos.button({
  ean: '1234567890123',
  language: ['nl', 'en']
});
```

You can also render thumbnails instead:
```javascript
Autheos.thumbnail({
  ean: '1234567890123',
  language: ['nl', 'en']
});
```

Or render videos directly into your page without buttons or carousel:
```javascript
Autheos.inline({
  ean: '1234567890123',
  language: ['nl', 'en']
});
```

You can specify a custom target element (works on all above methods):
```javascript
Autheos.button({
  ean: '1234567890123',
  language: ['nl', 'en']
}, document.querySelector('#example'));
```

For search results or multiple products
---------------------------------------

On search results pages or on pages where you want to display video for multiple
products, we provide convenient functions to perform a single API call for
different buttons. The options `Autheos.button`, `Autheos.thumbnail` and
`Autheos.inline` provide deferred counterparts: `Autheos.deferredButton`,
`Autheos.deferredThumbnail` and `Autheos.deferredInline` respectively. These
functions have the same signature as their "instant" counterparts.

```javascript
Autheos.deferredButton({
  ean: '1234567890123',
  language: ['nl', 'en']
});
```

This does not actually do anything yet. It creates an invisible placeholder for
where the button should go. With this you can place multiple placeholders in
different places for different products, and then invoke all deferred actions:

```javascript
Autheos.invokeDeferred();
```

This creates a batch request, firing a single API request instead of multiple.
By performing a single API call, you can drastically improve the performance of
the embed code.

Configuring templates
---------------------
(perhaps this should be a different topic?)

It may be useful to edit the templates used by the button, thumbnail and inline
options. You can do that globally like this:
```javascript
// For Autheos.button:
Autheos.defaults.buttonTemplate = '<some html template>';

// For Autheos.thumbnail
Autheos.defaults.thumbnailTemplate = '<some html template>';

// For Autheos.inline
Autheos.defaults.inlineTemplate = '<some html template>';
```
> Templates are in mustache format, read more about that here[link]

Or by creating a custom Autheos instance:
```javascript
const instance = Autheos.createInstance({
  // For instance.button:
  buttonTemplate: '<some html template>';

  // For instance.thumbnail
  thumbnailTemplate: '<some html template>';

  // For instance.inline
  inlineTemplate: '<some html template>';
});

instance.button({
  ean: '1234567890123',
  language: ['nl', 'en']
});
```

Or by passing configuration like this:
```javascript
Autheos.button({
  ean: '1234567890123',
  language: ['nl', 'en']
}, {
  buttonTemplate: '<some template>'
});
```
> The `button`, `thumbnail` and `inline` function, and their deferred
> counterparts accept 3 arguments. The second and third argument for target
> element and configuration are interchangeable.

Read about styling here[link]

If you want to embed videos directly look here[link]
