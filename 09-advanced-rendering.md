Advanced rendering
------------------

If you want to hook into the embed codes rendering process, this is the way to
go:

```javascript
Autheos.defaults.render = (type, results) => {
  const target = document.querySelector('#target');
  const button = someCreateButtonFunction();

  target.appendChild(button);

  button.addEventListener('click', () => showMyOwnCarousel(results));
}
```

you can use some autheos internal functions to make this process easier:
```javascript
// short list of autheos internal functions here, like what we use to generate
// buttons or thumbnails or how we create the carousel
```

for more info read the full api reference
