Direct embeds
-------------

Sometimes you just want to embed a single video directly. This can be useful
for brand pages and promotional pages for example. Direct embeds can done like
so:

```html
<script>Autheos.embedVideo(videoId);</script>
```

This will embed the video where you put the script. You can also specify a
custom target:

```javascript
Autheos.embedVideo(videoId, document.querySelector('#example'));
```

Want to configure player options like autoplay? You can pass along an object
with configuration.

```javascript
Autheos.embedVideo(videoId, {
  autoplay: true
});
```
> The second and third arguments are interchangeable. Read more about this
> behaviour in our [API reference][api-reference].

> You can find all player options in [configuring the embed code][configuration].

If you need more granular control, you can create a video element and insert it
into the DOM whenever you want.

```javascript
const video = Autheos.createVideo(videoId, {
  autoplay: true
});

document.querySelector('#example').appendChild(video);
```

> You can find all player options in [configuring the embed code][configuration].

[configuration]: 10-configuration.md
[api-reference]: 11-full-api-reference
