Conversion tracking
===================

With conversion tracking you can improve the quality of videos on your webshop.

> NOTE: This feature is required for A/B testing. A/B testing will not function
> without enabling conversion tracking.

You can track conversion using this:
```javascript
const button = document.querySelector('#add-to-cart');
Autheos.attachListener('add-to-cart', button);
```

You can also do multiple buttons:
```javascript
const button1 = document.querySelector('#add-to-cart-1');
const button2 = document.querySelector('#add-to-cart-2');
Autheos.attachListener('add-to-cart', [
  button1,
  button2
]);
```

`Autheos.attachHandler` will automatically attach the appropriate listeners to
the specified targets for the events `add-to-cart` and `maybe-more`. If the
event name is unknown to us, only click listeners are attached to the targets.

You can also do custom event tracking with this:

```javascript
Autheos.trackEvent('custom-event', {
  // Any arbitrary metadata
  foo: 'bar'
});
```

> for more info on that read full api reference
