![Autheos embed code][embedcode-logo]
=============================

[![Build Status][travis-badge]][travis-link]
[![npm package][npm-badge]][npm-link]

Increase your e-commerce sales by adding more product video content to your
shop.

Use the Autheos embed code just once to automatically retrieve product videos
from our database and instantly display them on all the correct product pages in
your shop.

Read our [quick start](01-quick-start.md) for an introduction!

> Already using an older version of the embed code?
> [Read our migration guide](12-migrating-from-1.0.md).

Documentation
-------------

- [Quick start](01-quick-start.md)
- [Installing](02-installing.md)
- [Embedding videos](03-embedding-videos.md)
- [Direct embeds](04-direct-embeds.md)
- [Conversion tracking](05-conversion-tracking.md)
- [Enabling A/B testing](06-enabling-ab-testing.md)
- [Styling the player](07-player-styling.md)
- [Styling the carousel](08-carousel-styling.md)
- [Configuring the embed code](09-configuration.md)
- [Advanced rendering options](10-advanced-rendering.md)
- [Full API reference](11-full-api-reference)
- [Migrating from 1.0](12-migrating-from-1.0.md)
- [Support](13-support.md)

[embedcode-logo]: https://cdn.autheos.com/logo/embedcode-logo.png
[travis-badge]: https://img.shields.io/travis/autheos/embedcode/master.svg
[travis-link]: https://travis-ci.org/autheos/embedcode
[npm-badge]: https://img.shields.io/npm/v/@autheos/embedcode.svg
[npm-link]: https://www.npmjs.com/package/@autheos/embedcode
